#! /bin/bash


#INIT - Startup Variable init
LINK="https://e-hentai.org/g/"
API="https://api.e-hentai.org/api.php"
ERR_CD="Key missing, or incorrect key provided."
POSTS="https://e-hentai.org/s/"
#!INIT

#JOB - Check link formatting & init ID and TOK
if [[ $(echo $1|sed -E "s~(http[s]{0,1})(:\/\/)([^/]*)(\/g\/)(.*)~\1\2\3\4~g") != $LINK ]];then ## Check for E-Hen format
	#ANCHOR - Print "link is not correct" and exit with 1
	echo "Not a E-Hen Post"
	exit 1
else
	#INIT - Init Gallary ID and TOK
	Gall_ID=$(echo $1|sed -E "s~(.*/g/)([0-9]*)(/)([a-z 0-9]*)(.*)~\2~g")	## Set Gallery ID
	Gall_TOK=$(echo $1|sed -E "s~(.*/g/)([0-9]*)(/)([a-z 0-9]*)(.*)~\4~g")	## Set Gallery Token
	#!INIT
fi
#!JOB

#exit 0 #STOP - Test exit 1

#INIT - JSON data to first send
INIT_JSON=$(jo -p method="gdata" gidlist="[$(jo -a $Gall_ID "$Gall_TOK" )]" namespace=1)	## Set send data to make a spi call
echo $INIT_JSON
#!INIT

echo -e "\nCurl 1\n"

#INIT - Store reply in POST_JSON using curl with INIT_JSON
POST_JSON=$(curl -s -X "POST" --json "$INIT_JSON" $API)		## Get POST_JSON respons from the api call
#!INIT


#INIT - Store keys names in JSON_KEYS from POST_JSON
JSON_KEYS=($(echo $POST_JSON|jshon -e gmetadata -e 0 -k ))	## Set the array of JSON_KEYS inside gmetadata-object from POST_JSON
#!INIT


#JOB - Check if JSON_KEYS has a error key
if [[ $(echo ${JSON_KEYS[1]}) = "error" ]];then		## Check if POST_JSON respons has error code
	#ANCHOR - print error and exit with 2
	echo "$(echo $POST_JSON|jshon -e gmetadata -e 0 -e "${JSON_KEYS[1]}" -u)"	## Print the error code
	echo $POST_JSON
	exit 2	## exit with return code 2
fi
#!JOB

echo "check 1"
#INIT - Get number of files from POST_JSON
FILE_CNT=$(echo $POST_JSON|jshon -e gmetadata -e 0 -e "filecount" -u)
echo $FILE_CNT
#!INIT


#INIT -  Get Number of pages in the gallary per 4 - posts
PAGE_CNT=$(expr $(expr $FILE_CNT / 40) + 1)
echo $PAGE_CNT
#!INIT

#SECTION - Sanity Check print ID__TOK and Title
echo -e "\n${Gall_ID}__$Gall_TOK\n"
echo "Title : $(echo $POST_JSON|jshon -e gmetadata -e 0 -e "${JSON_KEYS[3]}" -u)" 
#!SECTION

#INIT - Set Target Dir in TARGET_DIR
TARGET_DIR="${Gall_ID}__$Gall_TOK"
echo -e "\nTarget Dir = $TARGET_DIR\n"
#!INIT

#INIT - Set target HTML path
TAR_RT_HTML="./${TARGET_DIR}/${Gall_ID}__$Gall_TOK.html"
echo -e "\nTarget HTML = $TAR_RT_HTML\n"
#!INIT


#exit 2 #STOP - Test exit 2

#JOB - check if Dir and HTNL exist
if [[ -d "$TARGET_DIR" ]];then 	##	Check if there is'nt a file name with Gallery ID and Token
	if [[ ! -f "$TAR_RT_HTML" ]];then
		echo "Curl 2"
		curl -s "$1" -o "$TAR_RT_HTML"		##	Curl the url input to get the html page
		tidy -f /dev/null -qim -w "$TAR_RT_HTML"	##	Tidy the html and override
	fi
else
	mkdir "$TARGET_DIR"
	echo "Curl 3"
	curl -s "$1" -o "$TAR_RT_HTML"	
	tidy -f /dev/null -qim -w "$TAR_RT_HTML"
fi
#!JOB


#SECTION - Sanity check get title from html and print in JSON format
TITLE=$(cat "$TAR_RT_HTML"|grep -oP "\<title\>.*\<\/title\>"|sed -E 's/(<title>)(.*)(<\/title>)/\2/')
jo -p name="$TITLE"
echo -e "\nID = $ID\n"
#!SECTION


#JOB - Check if Post-Link array exist
if [[ ! -f "./${TARGET_DIR}/${Gall_ID}__$Gall_TOK.array" ]];then

	#JOB - Loop through Gallary Pages and get Post links
	POST_LINKS=""

	echo "Hello"

	POST_LINKS=( $(echo $(cat $TAR_RT_HTML|grep -oP "$POSTS[a-z 0-9]{10}/$Gall_ID-\d*")|sed -e 's~\n~ ~g') )

	#echo ${POST_LINKS[@]}

	for (( Cur_Page=1; Cur_Page<$PAGE_CNT; Cur_Page++))

		do
			echo "Hello$Cur_Page"
			TEMP_LIST=$(curl -s "${LINK}${Gall_ID}/${Gall_TOK}/?p=$Cur_Page") # -o "${TARGET_DIR}/${Cur_Page}_${Gall_ID}__$Gall_TOK.html"
			POST_LINKS=($(echo "${POST_LINKS[@]} $(echo $(echo $TEMP_LIST|grep -oP "$POSTS[a-z 0-9]{10}/$Gall_ID-\d*")|sed -e 's~\n~ ~g')"))
		done

	echo ${POST_LINKS[@]} > "./${TARGET_DIR}/${Gall_ID}__$Gall_TOK.array"
	#!JOB

else

	#ANCHOR - Exit if list already exist
	echo "List already exists"
	POST_LINKS=($(cat ./${TARGET_DIR}/${Gall_ID}__$Gall_TOK.array))

fi
#!JOB

#ANCHOR - Sanity Check print number of lines
echo ${POST_LINKS[@]}|sed -e "s~ ~\n~g"|sort|wc -l

#JOB - Loop through POST_LINKS and get full image request links
REGEN_FULL_LINK=""
read -p "Do you want to Re-Fetch full image links ? (y/n)" REGEN_CONF && [[ $REGEN_CONF == [yY] || $REGEN_CONF == [yY][eE][sS] ]] || exit 1
	for Cur_Link in ${POST_LINKS[@]}
	do
		#echo "$Cur_Link"
		REGEN_FULL_LINK=($(echo "${REGEN_FULL_LINK[@]} $(echo $(curl -s $Cur_Link | grep -Po "https://e-hentai\.org/fullimg\.php\?gid=\d*\&amp\;page=\d+\&amp\;key=[0-9 a-z]*") | sed "s~amp\;~~g")"))
		#echo ${REGEN_FULL_LINK[@]}
	done
echo ${REGEN_FULL_LINK[@]} > "./${TARGET_DIR}/${Gall_ID}__$Gall_TOK.full_size"
#!JOB